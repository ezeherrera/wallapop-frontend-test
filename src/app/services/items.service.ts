import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Item } from '../models/item.model';
import { Response } from '../models/response.model';
import { SearchInput, SortCriteriaInput } from '../models/search.model';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  private apiUrl = 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/items.json';
  private sourceData: Item[] = [];
  private store: BehaviorSubject<Item[]> =  new BehaviorSubject(this.sourceData);
  private searchFields: SearchInput = {};
  private sortCriterias: SortCriteriaInput[] = [
    { value: 'title', label: 'Título' },
    { value: 'description', label: 'Descripción' },
    { value: 'email', label: 'Email' },
    { value: 'price', label: 'Precio' },
  ];
  private selectedSortCriteria: SortCriteriaInput = this.sortCriterias[0]; // title by default

  public readonly items: Observable<Item[]> = this.store.asObservable();

  constructor(
    private http: HttpClient
  ) {
    this.loadData();
  }

  private fetchData(): Observable<Item[]> {
    if (!this.sourceData.length) {
      return this.http.get<Response>(this.apiUrl)
        .pipe(map(this.addIdToItems));
    }
    return of(this.sourceData);
  }

  private addIdToItems(response: Response): Item[] {
    return response.items
      .map((item, index) => ({
        id: index,
        ...item,
      }));
  }

  private sortAndDispatchItems(items: Item[]): void {
    const { value } = this.selectedSortCriteria;
    items.sort((a, b) => {
      if (a[value] > b[value]) {
        return 1;
      }
      if (a[value] < b[value]) {
        return -1;
      }
      return 0;
    });

    this.store.next(items);
  }

  private loadData(): void {
    this.fetchData().subscribe(
      data => {
        this.sourceData = data;
        this.sortAndDispatchItems(data);
      });
  }

  private isPriceInRange(price: number): boolean {
    const { min, max } = this.searchFields;
    const isOverMin = !min || (min && price >= min);
    const isUnderMax = !max || (max && price <= max);
    return isOverMin && isUnderMax;
  }

  private isTextMatch(item: Item): boolean {
    const { text } = this.searchFields;
    const compareString = text.toLocaleLowerCase();
    return (
      !text
      || item.title.toLocaleLowerCase().indexOf(compareString) > 0
      || item.description.toLocaleLowerCase().indexOf(compareString) > 0
      || item.email.indexOf(compareString) > 0
    );
  }

  private doSearch(): Observable<Item[]> {
    const currentItems = this.sourceData;
    const search = currentItems.filter(
      item => this.isTextMatch(item) && this.isPriceInRange(item.price)
    );

    return of(search);
  }

  public setSearchFields(input: SearchInput) {
    this.searchFields = input;
    this.doSearch().subscribe(
      results => this.sortAndDispatchItems(results)
    );
  }

  public getSortCriterias(): [SortCriteriaInput[], SortCriteriaInput] {
    return [this.sortCriterias, this.selectedSortCriteria];
  }

  public setSortCriteria(input: SortCriteriaInput) {
    this.selectedSortCriteria = input;
    const items = this.store.getValue();
    this.sortAndDispatchItems(items);
  }
}
