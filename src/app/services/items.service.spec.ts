import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ItemsService } from './items.service';
import { ItemsResponse } from '../models/item.model';

const mockResponse: ItemsResponse = {
  items: [
  {
    title: 'iPhone 6S Oro',
    description: 'Vendo un iPhone 6 S color Oro nuevo y sin estrenar. \
    Me han dado uno en el trabajo y no necesito el que me compré. \
    En tienda lo encuentras por 749 euros y yo lo vendo por 740. \
    Las descripciones las puedes encontrar en la web de apple. Esta libre.',
    price: 740,
    email: 'iphonemail@wallapop.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  }]
};

describe('ItemsService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
