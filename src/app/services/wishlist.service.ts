import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';

import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  private wishlist: Item[] = [];
  private store: BehaviorSubject<Item[]> =  new BehaviorSubject(this.wishlist);
  public readonly items: Observable<Item[]> = this.store.asObservable();

  constructor() { }

  addItem(item: Item) {
    this.wishlist.push(item);
  }

  removeItem(item: Item) {
    let position = -1;
    this.wishlist.some((wishlistItem, index) => {
      position = index;
      return wishlistItem.id === item.id;
    });
    if (position > -1) {
      this.wishlist.splice(position);
    }
  }

  getWishlist(): Observable<Item[]> {
    return of(this.wishlist);
  }
}
