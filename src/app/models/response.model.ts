interface ResponseItem {
  description: string;
  email: string;
  image: string;
  price: number;
  title: string;
}

export interface Response {
  items: ResponseItem[];
}
