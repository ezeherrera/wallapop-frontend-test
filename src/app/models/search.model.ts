export interface SearchInput {
  text?: string;
  min?: number;
  max?: number;
}

export interface SortCriteriaInput {
  value: string;
  label: string;
}
