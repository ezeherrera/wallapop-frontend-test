export interface Item {
  id: number;
  description: string;
  email: string;
  image: string;
  price: number;
  title: string;
}
