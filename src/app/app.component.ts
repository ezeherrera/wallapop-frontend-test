import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ItemsService } from './services/items.service';
import { WishlistService } from './services/wishlist.service';
import { Item } from './models/item.model';
import { SearchInput, SortCriteriaInput } from 'src/app/models/search.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  items: Observable<Item[]>;
  wishlist: Observable<Item[]>;
  search: SearchInput;
  opened = false;
  listSearchFields: SearchInput = {
    text: '',
    min: null,
    max: null,
  };
  listSortCriterias: SortCriteriaInput[] = [];
  listSelectedCriteria: SortCriteriaInput;

  constructor(
    private itemsService: ItemsService,
    private wishlistService: WishlistService,
  ) {
    [ this.listSortCriterias,
      this.listSelectedCriteria,
    ] = this.itemsService.getSortCriterias();
  }

  getItems(): void {
    this.items = this.itemsService.items;
  }

  getWishlist(): void {
    this.wishlist = this.wishlistService.items;
  }

  toggleSide() {
    this.opened = !this.opened;
    console.log(this.opened);
  }

  handleSearchBy(searchFields: SearchInput) {
    console.log(searchFields);
    this.itemsService.setSearchFields(searchFields);
  }
  handleSortBy(selectedCriteria: SortCriteriaInput) {
    this.itemsService.setSortCriteria(selectedCriteria);
  }

  ngOnInit() {
    this.getItems();
    this.getWishlist();
  }
}
