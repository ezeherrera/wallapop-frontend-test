import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SearchInput, SortCriteriaInput } from 'src/app/models/search.model';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() searchFields: SearchInput;
  @Input() selectedCriteria: SortCriteriaInput;
  @Input() sortCriterias: SortCriteriaInput[];

  @Output() searchBy = new EventEmitter<SearchInput>();
  @Output() sortBy = new EventEmitter<SortCriteriaInput>();

  setSearchFields(): void {
    this.searchBy.emit(this.searchFields);
  }

  setSortCriteria(): void {
    this.sortBy.emit(this.selectedCriteria);
  }

  ngOnInit(): void {
  }

}
