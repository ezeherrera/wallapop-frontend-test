import { Component, Input, OnInit } from '@angular/core';

import { WishlistService } from '../../services/wishlist.service';

import { Item } from '../../models/item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() item: Item;

  wishlisted = false;

  constructor(
    private wishlistService: WishlistService,
  ) { }

  ngOnInit(): void {
  }

  like(): void {
    if (!this.wishlisted) {
      this.wishlistService.addItem(this.item);
      this.wishlisted = true;
    } else {
      this.wishlistService.removeItem(this.item);
      this.wishlisted = false;
    }
  }
}
